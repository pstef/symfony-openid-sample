<?php

namespace App\Controller;

use Pstef\OpenId\OpenIdConnector;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SampleController
 * @package App\Controller
 */
class OpenIdSampleController extends AbstractController
{
  /**
   * OpenID Discovery sample page
   *
   * @Route("/", name="openid_discovery")
   *
   * @param Request $request
   * @param RouterInterface $router
   * @param OpenIdConnector $openIdConnector
   * @return \Symfony\Component\HttpFoundation\Response
   * @throws \Pstef\OpenId\CurlException
   * @throws \Pstef\OpenId\MissingDiscoveryDataException
   * @throws \Exception
   */
  public function openidDiscovery(Request $request, RouterInterface $router, OpenIdConnector $openIdConnector) {

    // This is needed to be able to check State and Nonce in the openidAuthComplete() afterwards
    $openIdConnector->saveSessionNonceAndState($request);

    // Uncomment the next instruction to override the default redirect URI
    // The URL http(s)://this.server.com/complete should be one of the configured redirect URIs
    // $openIdConnector->setExtraParameter('redirect_uri',
    //   $request->getSchemeAndHttpHost() . $router->generate('openid_complete'));

    return $this->render('sample/openid-discovery.html.twig', [
      'discovered' => $openIdConnector->discover(),
      'jwks' => $openIdConnector->getJWKS(),
      'authorization_url' => $openIdConnector->getAuthorizationEndpoint(),
      'discovery_url' => $openIdConnector->getDiscoveryEndpoint(),
      'end_session_url' => $openIdConnector->getEndSessionEndpoint(),
    ]);
  }

  /**
   * OpenID authentication complete URL
   *
   * @Route("complete", name="openid_complete")
   *
   * @param Request $request
   * @param OpenIdConnector $openIdConnector
   * @return \Symfony\Component\HttpFoundation\Response
   * @throws \Exception
   */
  public function openidAuthComplete(Request $request, OpenIdConnector $openIdConnector) {

    $session = $request->getSession();

    $args = [
      'parameters' => $request->request->all(),
      'token' => NULL,
      'nonce' => $session->get($openIdConnector->getSessionNonceKey()),
      'state' => $session->get($openIdConnector->getSessionStateKey()),
      'end_session_url' => $openIdConnector->getEndSessionEndpoint(),
    ];

    $id_token = $request->get('id_token');
    if ($id_token) {
      try {
        $args['decoded_token'] = $openIdConnector->decodeJwtToken($id_token);
      } catch (\Exception $e) {
        $args['decoded_token'] = NULL;
      }

//      try {
        $args['validated_token'] = $openIdConnector->validateAndDecodeToken($id_token);
//      } catch (\Exception $e) {
//        $args['error'] = $e->getMessage() . "\r\nTrace: " . $e->getTraceAsString();
//        $args['validated_token'] = NULL;
//      }

      if ($request->get('code')) {
        $openIdConnector->setAuthorizationCode($request->get('code'));

        // These parameters will be sent to the OpenId token endpoint:
        // we request access to the Graph API resource...
        $openIdConnector->setExtraParameter('resource', 'https://graph.microsoft.com/');
        // ...and to the 'user.read' and 'mail.read' scopes.
        $openIdConnector->setExtraParameter('scope', 'user.read mail.read');

        // Uncomment the next line to override the default redirect URI
        // $openIdConnector->setExtraParameter('redirect_uri', 'another_uri');

        $tokenResponse = $openIdConnector->visitTokenEndpoint();

        $args['token_params'] = $openIdConnector->getTokenEndpointParameters();
        $args['token_response'] = $tokenResponse;

        // Check token response integrity
        if (isset($tokenResponse['access_token'], $tokenResponse['token_type']) &&
          $tokenResponse['token_type'] === 'Bearer') {
          // We got an access token! Good. We can use it e.g. to call Microsoft Graph API endpoints.
          $args['token_ok'] = 1;
          // This is the endpoint for getting current user's information
          $url = 'https://graph.microsoft.com/v1.0/me';

          $graph = $openIdConnector->APIGet($url, $tokenResponse['access_token']);
          $args['graph_url'] = $url;
          $args['graph_response'] = $graph;
        }
      }
    }
    return $this->render('sample/openid-complete.html.twig', $args);
  }
}
