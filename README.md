# Sample OpenID implementation

A Symfony project implementing OpenId authentication flow (using the `pstef/openid` package).

The [Configuration](#Configuration) section explains how to implement Microsoft &reg; Azure AD 2.0 authorization flow.

## Requirements

- PHP **7.1.3** or higher (for Symfony)
  - **JSON** extension
  - **cURL** extension
- Composer (to install dependencies)
- An [Azure AD](https://azure.microsoft.com/en-us/services/active-directory/) subscription

## Installation

    $ composer install
    
## Configuration

### Setting up a Microsoft &reg; Azure AD Application

If you have an active Azure AD account, please follow the below steps to register and configure a new app (most of this section is taken from [Microsoft website](https://developer.microsoft.com/en-us/graph/docs/concepts/auth_register_app_v2)).

1. Sign in to the [Microsoft App registration portal](https://apps.dev.microsoft.com).
2. Choose **Add an app**.

   ❗ **_Note_**: If you signed in with a work or school account, select the _Add an app_ button for **Converged applications**.

3. Enter a name for the app and choose **Create application**.
   
   The registration page displays, listing the properties of your app.

4. Copy the application ID. This is the unique identifier for your app.

   You'll use the application ID to configure the app.
   
5. Under Platforms, choose **Add Platform**, and select **Web**.

   Make sure the _**Allow Implicit Flow**_ check box is selected.

    The **Allow Implicit Flow** option enables the OpenID Connect hybrid and implicit flows. 
    The hybrid flow enables the app to receive both sign-in info (the id token) and artifacts 
    (in this case, an authorization code) that the app uses to obtain an access token. 
    The hybrid flow is the default flow used by the OWIN OpenID Connect middleware. 
    For single page apps (SPA), the implicit flow enables the app to receive sign-in info and
     the access token.

6. Specify **http://localhost:8000/complete** as **Redirect URL**.
    
    The redirect URL is the location in your app that the Azure AD v2.0 endpoint calls when it has processed the
    authentication request.
        
    It's important to set the redirect URL to **http://localhost:8000/complete**, because that is the route associated to the 
    openidAuthComplete() action in the OpenIdSampleController of this project. This way, the OpenId response will be 
    received by the same controller which sent the request, and it can be verified and validated.
            
7. Under Application Secrets, choose **Generate New Password**.
 
   Copy the app secret from the _**New password generated**_ dialog box and save it somewhere.
    
   ⚠ **Important**: copy the app secret **_before_** you close the _New password generated_ dialog. 
   After you close the dialog, there is no way to retrieve the secret (that is, you'll have to delete
   it and generate another one).

8. Choose **Save**.

### Almost there...

This project uses the below environment variables - as long as you run Symfony in development environment, those can 
be set in the `.env` file.

- `openid_client_id`: ID of your application, assigned by the OpenId provider. If you followed the setup so far, it's 
   the application ID you copied in step 4. 

- `openid_discovery`: URL of the OpenId provider's discovery page. For Azure AD, it must be one of the below.

    - If you know your tenant ID (please replace **TENANT_ID** with the actual value): 
    
      https://login.microsoftonline.com/TENANT_ID/.well-known/openid-configuration 
    
    - Otherwise: 
    
      https://login.microsoftonline.com/common/.well-known/openid-configuration
    

- `openid_domain` (**can be blank**): for Azure AD, the domain of your active directory. 
   Its value will be used as the **domain_hint** argument for the authorization URL; if it's not 
   blank, you'll skip the Microsoft Login discovery page and you'll be taken to your organization's
   login page directly.

- `openid_redirect_uri` (**can be blank**): the URL to call when the authorization endpoint has processed the 
  request. If you followed the setup so far, it's the redirect URL you saved in step 6. 
  If you leave this variable blank, the OpenId backend will use whatever redirect URI you configured 
  in your application; if you configured more than one, the backend will use the first one.

- `openid_client_secret`: password/secret key that you created for your application. If you followed 
   the setup so far, it's the **password** you saved in step 7. 


## Usage

The project can be run using the built-in PHP webserver:

    $ cd symfony-openid-sample  
    $ php bin/console server:run   

The OpenId discovery page will be available at [localhost:8000](http://localhost:8000/) .

If the configuration is ok, the page will retrieve the discovery data from the OpenId provider and show all the info, 
along with a clickable **Authorization (login) endpoint** and an **End session (logout) endpoint**.

Selecting the **Authorization (login) endpoint** will start the OpenId authorization flow.

When the Authorization endpoint has processed the request, it will head your browser to the proper
redirect URL (either the one specified in the application configuration, or the one you specified 
in the `.env` file), which defaults to [localhost:8000/complete](http://localhost:8000/complete) .

That page tries the following operations:

1. if it received an `id_token` parameter, it tries to parse it as a JWT **identity** token and validate it 
   against the saved session variable 'openid_nonce' (saved when the OpenId discovery page, 
   [localhost:8000](http://localhost:8000/), is visited);
2. if it received a `state` parameter, it tries to match it with the saved session variable 
   'openid_state' (saved when the OpenId discovery page, [localhost:8000](http://localhost:8000/),
   is visited);
3. if it received a `code` parameter, it tries to use its value to request an **access** token by
   POSTing the proper parameters to the OpenId provider's token endpoint. If it succeeds, it uses 
   the returned access token to visit a Microsoft Graph API endpoint, and shows the results.
   
   Both the token request and the Graph API request are done using cURL. 

## Documentation

This Symfony app uses the **Pstef\OpenId\OpenIdConnector** class to perform all the OpenId-related operations.
This class is made available as a Symfony service, auto-wired in the `config/services.yaml` configuration file,
as follows:

```yaml
  # Autowire OpenIdConnector using environment variables as constructor args
  Pstef\OpenId\OpenIdConnector:
    public: true
    arguments:
      $discoveryEndpoint: '%env(openid_discovery)%'
      $clientId: '%env(openid_client_id)%'
      $clientSecret: '%env(openid_client_secret)%'
      # The $extraParams and $redirectUri arguments are optional and can be commented out if you wish
      $extraParams:
        domain_hint: '%env(openid_domain)%'
      $redirectUri: '%env(openid_redirect_uri)%'
 ```

I chose to use environment variables so that client ID and client secret won't accidentally get 
committed to your VCS; you can change this behaviour to whatever fits your needs.

### API documentation

@todo


### Disclaimer

I'm not working for Microsoft, nor I am _in any way_ endorsing Microsoft products. This repository has not been approved or 
revised by Microsoft and/or any of its affiliates.

&copy; 2018 Paolo Stefan.